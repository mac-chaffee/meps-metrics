"""
Code taken from Django's standard crypto functions and utilities.
View LICENSE_DJANGO for licensing details
"""
import base64
import hashlib
import hmac
import os
import random
import time

from falcon import HTTPUnauthorized

from settings import SESSION_LENGTH
from connections import get_redis_connection


SECRET_KEY = os.getenv('MEPS_SECRET_KEY', 'placeholder')


# Use the system PRNG if possible
try:
    random = random.SystemRandom()
    using_sysrandom = True
except NotImplementedError:
    import warnings
    warnings.warn('A secure pseudo-random number generator is not available '
                  'on your system. Falling back to Mersenne Twister.')
    using_sysrandom = False


def salted_hmac(key_salt, value, secret=None):
    """Return the HMAC-SHA1 of 'value', using a key generated from key_salt and a
    secret (which defaults to SECRET_KEY).

    A different key_salt should be passed in for every application of HMAC.
    """
    if secret is None:
        secret = SECRET_KEY

    key_salt = key_salt.encode('utf-8', 'strict')
    secret = secret.encode('utf-8', 'strict')

    # We need to generate a derived key from our base key.  We can do this by
    # passing the key_salt and our base key through a pseudo-random function and
    # SHA1 works nicely.
    key = hashlib.sha1(key_salt + secret).digest()

    # If len(key_salt + secret) > sha_constructor().block_size, the above
    # line is redundant and could be replaced by key = key_salt + secret, since
    # the hmac module does the same thing for keys longer than the block size.
    # However, we need to ensure that we *always* do this.
    return hmac.new(key, msg=value.encode('utf-8', 'strict'), digestmod=hashlib.sha1)


def get_random_string(length=12,
                      allowed_chars='abcdefghijklmnopqrstuvwxyz'
                                    'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    """
    Return a securely generated random string.

    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """
    if not using_sysrandom:
        # This is ugly, and a hack, but it makes things better than
        # the alternative of predictability. This re-seeds the PRNG
        # using a value that is hard for an attacker to predict, every
        # time a random string is required. This may change the
        # properties of the chosen random sequence slightly, but this
        # is better than absolute predictability.
        random.seed(
            hashlib.sha256(
                ('%s%s%s' % (random.getstate(), time.time(), SECRET_KEY)).encode()
            ).digest()
        )
    return ''.join(random.choice(allowed_chars) for i in range(length))


def constant_time_compare(val1, val2):
    """Return True if the two strings are equal, False otherwise."""
    return hmac.compare_digest(val1.encode('utf-8', 'strict'), val2.encode('utf-8', 'strict'))


def pbkdf2(password, salt, iterations, dklen=0, digest=None):
    """Return the hash of password using pbkdf2."""
    if digest is None:
        digest = hashlib.sha256
    if not dklen:
        dklen = None
    password = password.encode('utf-8', 'strict')
    salt = salt.encode('utf-8', 'strict')
    return hashlib.pbkdf2_hmac(digest().name, password, salt, iterations, dklen)


def mask_hash(hash, show=6, char="*"):
    """Return the given hash, with only the first ``show`` number shown. The
    rest are masked with ``char`` for security reasons.
    """
    masked = hash[:show]
    masked += char * len(hash[show:])
    return masked


class PasswordHasher:
    """Secure password hashing using the PBKDF2 algorithm (recommended)
    Configured to use PBKDF2 + HMAC + SHA256.
    The result is a 64 byte binary string.  Iterations may be changed
    safely but you must rename the algorithm if you change SHA256.
    """
    algorithm = "pbkdf2_sha256"
    iterations = 100000
    digest = hashlib.sha256
    library = None

    def salt(self):
        """Generate a cryptographically secure nonce salt in ASCII."""
        return get_random_string()

    def encode(self, password, salt, iterations=None):
        assert password is not None
        assert salt and '$' not in salt
        if not iterations:
            iterations = self.iterations
        hash = pbkdf2(password, salt, iterations, digest=self.digest)
        hash = base64.b64encode(hash).decode('ascii').strip()
        return "%s$%d$%s$%s" % (self.algorithm, iterations, salt, hash)

    def verify(self, password, encoded):
        algorithm, iterations, salt, hash = encoded.split('$', 3)
        assert algorithm == self.algorithm
        encoded_2 = self.encode(password, salt, int(iterations))
        return constant_time_compare(encoded, encoded_2)


def session_auth(request, response, **kwargs):
    # Pull the session_id from the cookies if it exists
    session_id = request.cookies.get('session_id')
    if not session_id:
        raise HTTPUnauthorized("Authentication Required",
                               "The request had no session_id cookie")

    # Check to redis to see if the session exists
    redis = get_redis_connection()
    username = redis.get(session_id)
    if not username:
        raise HTTPUnauthorized("Authentication Invalid",
                               "That session_id does not exist (or it expired)")

    # Refresh the TTL of the session
    redis.expire(session_id, SESSION_LENGTH)

    request.context['username'] = username.decode()
