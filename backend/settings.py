import os


DEBUG = bool(int(os.getenv('MEPS_DEBUG', '1')))

SESSION_LENGTH = 60 * 60
