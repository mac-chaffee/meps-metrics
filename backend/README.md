## MEPS Backend

This folder contains the API that the front-end talks to via REST calls.

### Installing

**Python stuff:**
```
virtualenv -p python3.6 venv
source venv/bin/activate

pip install -r requirements.txt
hug -m api
```

**Redis stuff:**
```
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable/
make
make test
sudo make install
redis-server
```
Make sure it worked:
```
redis-cli
127.0.0.1:6379> ping
PONG
```

## Schema
This is how data will be represented in Redis:

```python
schema = {
    # Stored as a HASH
    "username1": {
        "email": "email@example.com",
        "password": "pbkdf2 hashed password"
    },
    # Stored as a SORTED SET
    "username1__meps_data": [
        # Stored as strings, score=epoch
        {
            "m": 10,
            "m_comment": "encrypted text",
            "e": 10,
            "e_comment": "encrypted text",
            "p": 10,
            "p_comment": "encrypted text",
            "s": 10,
            "s_comment": "encrypted text",
            "total": 40,
            "journal": "encrypted text"
        },
        {
            "m": 10,
            "m_comment": "encrypted text",
            "e": 10,
            "e_comment": "encrypted text",
            "p": 10,
            "p_comment": "encrypted text",
            "s": 10,
            "s_comment": "encrypted text",
            "total": 40,
            "journal": "encrypted text"
        }
    ],
    # Sessions (expire after settings.SESSION_LENGTH)
    "298a47a2-07dc-4d6b-95a4-c3aa1c3f9146": "user1",
    "a7a3df2f-b25a-4ccb-b114-16ae5e9c8edb": "user2",
}

```

## Docker

```bash
docker login registry.gitlab.com

docker build -t registry.gitlab.com/mac-chaffee/meps-metrics/meps-metrics-api backend/
docker push registry.gitlab.com/mac-chaffee/meps-metrics/meps-metrics-api
```
