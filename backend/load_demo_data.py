import json
import random
import time
from datetime import datetime, timedelta
from connections import get_redis_connection


redis = get_redis_connection()

# Clear out existing data
redis.delete('mac__meps_data')

# Create dates near today
today = datetime.today()
dates = []

for i in range(365):
    dates.append(today - timedelta(days=i))


# Create fake entries on each date
for date in dates:
    key = 'mac__meps_data'
    entries = [random.randint(1, 10) for i in range(4)]
    data = {
        'm': entries[0],
        'm_comment': 'comment',
        'e': entries[1],
        'e_comment': 'comment',
        'p': entries[2],
        'p_comment': 'comment',
        's': entries[3],
        's_comment': 'comment',
        'total': sum(entries),
        'journal': 'journal entry'
    }
    epoch = int(time.mktime(date.timetuple())) * 1000

    data['total'] = sum(val for val in data.values() if isinstance(val, int))
    print(f"Adding data on {date.date()}")
    redis.zadd(key, {json.dumps(data): epoch})
