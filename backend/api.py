import json
import os
import uuid

import hug
from falcon import HTTP_400
import redis

from connections import get_redis_connection
from security import session_auth, PasswordHasher
from settings import DEBUG, SESSION_LENGTH


@hug.get()
def health():
    return True


@hug.post()
def register(username: hug.types.text, email: hug.types.text, password: hug.types.text, response):
    """Encrypt the password and save everything to redis
     curl -iX POST -H "Content-Type: application/json" \
        -d '{"username": "mac", "email": "mac@example.com", "password": "secret"}' \
        localhost:8000/register
    """
    connection = get_redis_connection()

    if connection.exists(username):
        response.status = HTTP_400
        return {"error": "That username is taken"}

    hasher = PasswordHasher()
    encrypted_pass = hasher.encode(password, hasher.salt())

    user_data = {
        "email": email, "password": encrypted_pass
    }
    connection.hmset(username, user_data)


@hug.post()
def login(username: hug.types.text, password: hug.types.text, response):
    """Check the password and if it's valid, create a session_id
    curl -iX POST -H "Content-Type: application/json" \
        -d '{"username": "mac", "password": "secret"}' \
        localhost:8000/login
    """
    # Get the hashed password if it exists
    connection = get_redis_connection()
    expected_hashed_password = connection.hget(username, 'password')
    if expected_hashed_password is None:
        return {"status": False}

    # Compare the hashes
    hasher = PasswordHasher()
    is_correct = hasher.verify(password, expected_hashed_password.decode('UTF-8'))

    # Create, save, and set the session_id cookie if the password worked
    if is_correct:
        session_id = str(uuid.uuid4())
        connection.set(session_id, username, ex=SESSION_LENGTH)
        response.set_cookie(
            'session_id',
            session_id,
            secure=False if DEBUG else True,
            max_age=SESSION_LENGTH
        )

    return {"status": is_correct}


@hug.post(requires=session_auth)
def add_data(epoch: hug.types.number, data: hug.types.json, request):
    """
    curl -iX POST -H "Content-Type: application/json" \
        -d '{"epoch": 1508551816133, "data": {"m": 10, "m_comment": "good"}}' \
        --cookie "session_id=${MEPS_SESSION_ID}" \
        localhost:8000/add_data
    """
    redis = get_redis_connection()
    username = request.context['username']
    key = f'{username}__meps_data'

    data['total'] = sum(val for val in data.values() if isinstance(val, int))
    redis.zadd(key, {json.dumps(data): epoch})


@hug.get(requires=session_auth)
def get_data(epoch: hug.types.number, request):
    """
    curl -iX GET -H "Content-Type: application/json" \
        -d '{"epoch": 1508551816133}' \
        --cookie "session_id=${MEPS_SESSION_ID}" \
        localhost:8000/get_data
    """
    redis = get_redis_connection()
    username = request.context['username']
    key = f'{username}__meps_data'

    # Redis only supports accessing ordered sets by a range of scores
    data_list = redis.zrangebyscore(key, epoch, epoch)
    if data_list:
        data = json.loads(data_list[0])
        data['epoch'] = epoch
        return data


@hug.get(requires=session_auth)
def get_all_data(request):
    """
    curl -iX GET -H "Content-Type: application/json" \
        --cookie "session_id=${MEPS_SESSION_ID}" \
        localhost:8000/get_all_data
    """
    redis = get_redis_connection()
    username = request.context['username']
    key = f'{username}__meps_data'
    all_data = redis.zrange(key, 0, -1, withscores=True)

    # Convert the list of tuples into a list of dicts
    # by putting the score into the json string
    all_combined_data = []
    for data, score in all_data:
        data = json.loads(data)
        data['epoch'] = int(score)
        all_combined_data.append(data)
    return all_combined_data
