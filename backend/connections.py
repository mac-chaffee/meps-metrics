import os
import redis


def get_redis_connection():
    return redis.StrictRedis(
        host=os.environ.get("REDIS_HOSTNAME", "localhost"),
        port=int(os.environ.get("REDIS_PORT", "6379")),
        password=os.environ.get("REDIS_PASSWORD")
    )
