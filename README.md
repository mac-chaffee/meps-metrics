# MEPS-Metrics: A self-help web-app

MEPS stands for Mental, Emotional, Physical, and Spiritual. This site will let you
rank yourself in these four categories every day. You will be provided with
resources and tips for improving your rankings, as well as data analytics based on your
past rankings.

## Under the hood

**Front-end:** React

**Back-end:** Python

**Hosting:** DigitalOcean Kubernetes

**Storage:** Redis

## Development setup

1. Install NodeJS, npm, and docker:
    ```bash
    sudo apt install nodejs npm

    sudo yum install docker
    sudo usermod -a -G docker $(whoami)

    <logout and back in>

    sudo service docker start
    docker login registry.gitlab.com
    ```

2. Clone the repo, install dependencies
    ```
    git clone git@gitlab.com:mac-chaffee/meps-metrics.git
    npm install
    ```

3. Start the development server
    ```
    npm run start
    ```

## Deployment

1. First update the version in `run.sh` and in `infra/charts/meps-metrics/Chart.yaml`

2. Build and push the docker images
```bash
docker login registry.gitlab.com
(need a personal access token to login)

./run deployImages
```

3. Follow the directions in the [infrastructure](https://gitlab.com/mac-chaffee/infrastructure) repo
