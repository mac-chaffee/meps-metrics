import React from 'react';


class SignupForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSignup = this.handleSignup.bind(this);
        this.state = {password: '', password2: ''};
    }

    handleChange(e) {
        this.setState({[e.target.id]: e.target.value}, this.checkPasswordsMatch);
        // Clear the username error if they changed it
        if (e.target.id === 'username') {
            e.target.classList.remove('has-error');
        }
    }

    /** Make sure the passwords match if they've been filled in **/
    checkPasswordsMatch() {
        if (this.state.password && this.state.password2) {
            if (this.state.password !== this.state.password2) {
                document.querySelector("#password").classList.add('has-error');
                document.querySelector("#password2").classList.add('has-error');
                return false;
            }
        }
        document.querySelector("#password").classList.remove('has-error');
        document.querySelector("#password2").classList.remove('has-error');
        return true;
    }

    /** POST the user's credentials and send them to the login page */
    handleSignup(event) {
        event.preventDefault();
        if (this.checkPasswordsMatch()) {
            fetch('/api/register', {
                method: 'POST',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(this.state)
            // Send them to the login page unless there was an error
            }).then(response => {
                if (!response.ok) {
                    throw Error(response);
                }
            }).then(() => {
                this.props.history.push('/login')
            }).catch(() => {
                document.querySelector("#username").classList.add('has-error');
            });
        }

    }

    render() {
        return (
            <form className="centered" onSubmit={this.handleSignup}>
                <label>Username</label>
                <input id="username" type="text" required onChange={this.handleChange}/>
                <label>Email</label>
                <input id="email" type="email" required onChange={this.handleChange}/>
                <label>Password</label>
                <input id="password" type="password" required onChange={this.handleChange}/>
                <label>Password (again)</label>
                <input id="password2" type="password" required onChange={this.handleChange}/>

                <button className="btn" type="submit">Sign up</button>
            </form>
        );
    }
}

export default SignupForm;
