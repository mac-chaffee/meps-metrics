import React from 'react';
import { Link } from 'react-router-dom'
import '../css/Fluff.css'


class Fluff extends React.Component {
    render() { return (
        <div className="Fluff centered">
            <p>The MEPS Metric is a self-help system that lets you track your
                Mental, Emotional, Physical, and Spiritual health over time.
            </p>
            <p>
                Rate yourself out of 10 in each of the four MEPS categories
                every day to keep track of your well-being.
            </p>
            <Link to="/signup" className="btn">Sign up</Link>
            <br/>
            <Link to="/login" className="btn">Log in</Link>
        </div>
    );
    }
}

export default Fluff;
