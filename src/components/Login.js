import React from 'react';


class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleAuth = this.handleAuth.bind(this);
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    /** POST the user's credentials to the backend and log them in if it worked */
    handleAuth(event) {
        event.preventDefault();
        fetch('/api/login', {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state)
        // Parse the json you get back and return a promise
        }).then((response) => { return response.json() }
        // When parsing is done, check the result and log them in if needed
        ).then((json) => {
            if (json.status === true) {
                this.props.history.push('/rate');
            } else {
                alert('Invalid username or password')
            }
        });
    }

    render() {
        return (
            <form className="centered" onSubmit={this.handleAuth}>
                <label>Username</label>
                <input name="username" type="text" onChange={this.handleChange}/>
                <label>Password</label>
                <input name="password" type="password" onChange={this.handleChange}/>
                <button className="btn" type="submit">Log in</button>
            </form>
        );
    }
}

export default LoginForm;
