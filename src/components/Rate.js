import React from 'react';


class RateForm extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.saveRatings = this.saveRatings.bind(this);
    }

    handleChange(e) {
        if (e.target.type === 'number') {
            this.setState({[e.target.name]: parseInt(e.target.value, 10)});
        } else {
            this.setState({[e.target.name]: e.target.value});
        }
    }

    saveRatings(event) {
        event.preventDefault();
        if (this.state === null) {
            alert("You need to fill out at least one of the fields");
            return;
        }
        let payload = {
            "epoch": new Date().getTime(),
            "data": this.state
        };
        console.log(payload);
        fetch('/api/add_data', {
            method: 'POST',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(payload)
        }).then((result) => this.props.history.push('/dashboard'))
    }

    render() {
        return (
            <form className="centered" onSubmit={this.saveRatings}>
                <label>Mental</label>
                <input name="m" type="number" onChange={this.handleChange}/>

                <label>Emotional</label>
                <input name="e" type="number" onChange={this.handleChange}/>

                <label>Physical</label>
                <input name="p" type="number" onChange={this.handleChange}/>

                <label>Spiritual</label>
                <input name="s" type="number" onChange={this.handleChange}/>
                <button className="btn" type="submit">Save</button>
            </form>
        );
    }
}

export default RateForm;
