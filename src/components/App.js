import React from 'react';
import { Route } from 'react-router-dom'
import '../css/App.css'
import SignupForm from "./Signup";
import LoginForm from "./Login";
import RatingForm from "./Rate";
import Dashboard from "./Dashboard";
import Fluff from "./Fluff";


class App extends React.Component {
    render() { return (
        <div className="App centered">
            <Route exact path="/" component={Fluff}/>
            <Route path="/login" component={LoginForm}/>
            <Route path="/signup" component={SignupForm}/>
            <Route path="/rate" component={RatingForm}/>
            <Route path="/dashboard" component={Dashboard}/>
        </div>
    );
    }
}

export default App;
