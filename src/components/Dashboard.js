import React from 'react';
import '../css/Dashboard.css'
import Plotly from 'plotly.js/dist/plotly-basic';

function createGraph(data) {
    if (data.length === 0) {
        document.querySelector('#meps-data').innerText = "You haven't entered any data yet.";
        return
    }
    // Convert all epochs to dates
    data.forEach(function(d) {
        d.date = new Date(d.epoch);
    });

    let colors = {'m': '#9a5248', 'e': '#c66e48', 'p': '#406384', 's': '#e8bb6b'};

    let combined_data = [];

    ['Spiritual', 'Physical', 'Emotional', 'Mental'].forEach((attr) => {
        let letter = attr[0].toLowerCase();
        combined_data.push( {
            x: data.map((d) => {return d.date}),
            y: data.map((d) => {return d[letter]}),
            name: attr,
            marker: {color: colors[letter]},
            type: 'bar'
        })
    });

    let layout = {
        barmode: 'stack',
        font: {color: 'white'},
        paper_bgcolor: 'rgba(0,0,0,0)',
        plot_bgcolor: 'rgba(0,0,0,0)',
        xaxis: {
            showgrid: false,
            tickformat: "%b %e",
        },
        yaxis: {showgrid: false, zeroline: false},
        hoverlabel: {bordercolor: 'rgba(0,0,0,0)', },
        legend: {
            x: 0.5, y: 1.1, orientation: 'h', xanchor: 'center'
        },
        margin: {
            t: 20,
            l: 20,
            r: 20,
            b: 20
        }
    };

    Plotly.newPlot('meps-data', combined_data, layout);
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}


class Dashboard extends React.Component {
    componentDidMount() {
        fetch(`/api/get_all_data`, {
            method: 'GET',
            credentials: 'same-origin',
            headers: {'Content-Type': 'application/json'},
        })
        .then(handleErrors)
            // Parse the json you get back and return a promise
        .then((response) => { return response.json() })
            // When parsing is done, create the graph
        .then(createGraph)
        .catch((err) => {
            console.log(err);
            this.props.history.push('/login')
        });
    }

    render() {
        return (
            <div id="meps-data" style={{width: '80vw'}}>

            </div>
        );
    }
}

export default Dashboard;
