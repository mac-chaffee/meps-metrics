FROM nginx
COPY build /usr/share/nginx/html
# nginx.conf is configured in infra/charts/meps-metrics/templates/nginx-configmap.yaml

CMD ["nginx", "-g", "daemon off;"]
